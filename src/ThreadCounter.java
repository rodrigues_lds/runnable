import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * This class is used to control the Atomic variable, in this case, a counter.
 */
class Counter {

    // Integer Atomic variable that will be shared among the threads.
    private AtomicInteger atomicInteger = new AtomicInteger();

    /**
     * This function increments the atomic variable.
     */
    public void increment() {
        atomicInteger.getAndIncrement();
    }

    /**
     * This function gets the atomic integer value.
     *
     * @return the atomic value
     */
    public int getAtomicInteger() {
        return atomicInteger.intValue();
    }
}

/**
 * Requirement: Use of a Thread - a class that extends the Thread class.
 */
class ThreadCounter extends Thread {

    private Counter counter;
    private int start;

    /**
     * Non-default constructor
     *
     * @param c     is the Counter object
     * @param start defines the countdown start point
     */
    public ThreadCounter(Counter c, Integer start) {
        this.counter = c;
        this.start = start;
    }

    /**
     * This function run the code when the thread starts.
     */
    public void run() {

        // Random will specify a delay later to better visualize the threads running.
        Random time = new Random();
        // The following line of code will get the thread ID in order to better visualize the threads running.
        Long threadID = Thread.currentThread().getId();
        System.out.println("*** Thread ID " + threadID + " has started! ***");

        // It will countdown and show the value as well as increment the atomic variable.
        for (int i = this.start; i > 0; i--) {
            System.out.println("Thread ID: " + Thread.currentThread().getId() + " | Counter: " + i);
            counter.increment();
            try {
                Thread.sleep(time.nextInt(4000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        // Result: Each time that it iterates, a new value is displayed for the thread.

        System.out.println("*** Thread ID " + threadID + " has finished! ***");
    }
}
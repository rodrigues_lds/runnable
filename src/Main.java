import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

    /**
     * This function defines the start point of the program
     *
     * @param args arguments to pass by the console
     */
    public static void main(String[] args) {

        Counter counter = new Counter();
        // Calling a class that extends the Thread class.
        System.out.println("\nThreads with atomic value will start manually!");
        ThreadCounter thread1 = new ThreadCounter(counter, 10);
        ThreadCounter thread2 = new ThreadCounter(counter, 5);
        // Start() method causes the threads to begin execution, the JVM calls the run method of these threads.
        // Requirement: Start your Thread class manually.
        thread1.start();
        thread2.start();
        // Result: The result is that two threads are running concurrently.
        try {
            // The join method allows one thread to wait for the completion of another. It was used to separate the requirements.
            thread1.join();
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // Because the threads were joined, it is possible to evaluate the atomic integer value after they are finished.
        // Requirement: Use an Atomic Variable in either your Thread or Runnable.
        System.out.println("The Atomic value is: " + counter.getAtomicInteger());
        // Result: The threads will run at the same time and will be finished in different time.
        // Result: Since thread1 starts with 10 and thread2 starts with 5, the atomic integer value is 15.


        // Calling a class that implements the Runnable interface.
        System.out.println("\nRunnable will start manually!");
        Runnable runnable1 = new RunnableCounter(6);
        Runnable runnable2 = new RunnableCounter(3);
        Thread thread3 = new Thread(runnable1);
        Thread thread4 = new Thread(runnable2);
        // Start() method causes the threads to begin execution, the JVM calls the run method of these threads.
        // Requirement: Start your Runnable class manually.
        thread3.start();
        thread4.start();
        // Result: The result is that two threads are running concurrently.
        try {
            // The join method allows one thread to wait for the completion of another. It was used to separate the requirements.
            thread3.join();
            thread4.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // Result: The threads will run at the same time and will be finished in different time.

        // Requirement: Run at least one of your Thread or Runnable using an Executor.
        System.out.println("\nRunnable using an Executor will start!");
        ExecutorService executor = Executors.newFixedThreadPool(2);
        // Result: Two threads will be executed at the same time.
        Runnable runnable3 = new RunnableCounter(4);
        Runnable runnable4 = new RunnableCounter(2);
        // It starts the execution of the threads.
        executor.execute(runnable3);
        executor.execute(runnable4);
        // It shutdown the executor.
        executor.shutdown();
        // Result: The threads will run at the same time and will be finished in different time.
    }
}

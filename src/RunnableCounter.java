import java.util.Random;

/**
 * Requirement: Use of a Runnable - a class that implements the Runnable interface.
 */
class RunnableCounter implements Runnable {

    private int start;

    /**
     * Non-default constructor
     *
     * @param start defines the countdown start point
     */
    public RunnableCounter(int start) {
        this.start = start;
    }

    @Override
    public void run() {
        // Random will specify a delay later to better visualize the threads running.
        Random time = new Random();
        // The following line of code will get the thread ID in order to better visualize the threads running.
        Long threadID = Thread.currentThread().getId();
        System.out.println("*** Runnable ID " + threadID + " has started! ***");

        // It will countdown and show the value of variable while the thread is running.
        for (int i = this.start; i > 0; i--) {
            System.out.println("Runnable ID: " + threadID + " | Counter: " + i);
            try {
                Thread.sleep(time.nextInt(10000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        // Result: Each time that it iterates, a new value is displayed for the thread.

        System.out.println("*** Runnable ID " + threadID + " has finished! ***");
    }
}